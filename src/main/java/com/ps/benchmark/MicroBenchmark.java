package com.ps.benchmark;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.jmx.JmxConfig;
import io.micrometer.jmx.JmxMeterRegistry;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.BenchmarkParams;
import org.openjdk.jmh.infra.ThreadParams;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Threads(5)
@Fork(4)
public class MicroBenchmark {
    static final int THREAD_SLICE = 1000;

    private MeterRegistry jmxRegistry;
    private MeterRegistry emptyRegistry;

    private ConcurrentMap<String, String> defaultMap;
    private ConcurrentMap<String, String> mapWithEmptyMetrics;
    private ConcurrentMap<String, String> mapWithJmxMetrics;


    @Setup
    public void setup(BenchmarkParams params) {
        int capacity = 16 * THREAD_SLICE * params.getThreads();
        emptyRegistry = new CompositeMeterRegistry();
        jmxRegistry = new JmxMeterRegistry(new JmxCustomConfig(), Clock.SYSTEM);

        defaultMap = getNewMap(capacity, params.getThreads());
        mapWithEmptyMetrics = emptyRegistry.gaugeMapSize("jmxMetricsMap", Tags.empty(), getNewMap(capacity, params.getThreads()));
        mapWithJmxMetrics = jmxRegistry.gaugeMapSize("jmxMetricsMap", Tags.empty(), getNewMap(capacity, params.getThreads()));
    }

    /*
     * Here is another neat trick. Generate the distinct set of keys for all threads:
     */

    @State(Scope.Thread)
    public static class Ids {
        private List<String> ids;

        @Setup
        public void setup(ThreadParams threads) {
            ids = new ArrayList<>(THREAD_SLICE);
            for (int c = 0; c < THREAD_SLICE; c++) {
                ids.add("ID" + (THREAD_SLICE * threads.getThreadIndex() + c));
            }
        }
    }

    @Benchmark
    public void measureDefaultMap(Ids ids) {
        for (String s : ids.ids) {
            defaultMap.remove(s);
            defaultMap.put(s, s);
        }
    }

    @Benchmark
    public void measureMapWithEmptyMetrics(Ids ids) {
        for (String s : ids.ids) {
            mapWithEmptyMetrics.remove(s);
            mapWithEmptyMetrics.put(s, s);
        }
    }

    @Benchmark
    public void measureMapWithJmxMetrics(Ids ids) {
        for (String s : ids.ids) {
            mapWithJmxMetrics.remove(s);
            mapWithJmxMetrics.put(s, s);
        }
    }

    /*
     * ============================== HOW TO RUN THIS TEST: ====================================
     *
     * You can run this test:
     *
     * a) Via the command line:
     *    $ mvn clean install
     *    $ java -jar target/benchmarks.jar ".*MicroBenchmark.*" -wi 5 -i 5 -t 4 -f 5
     *    (we requested 5 warmup iterations, 5 iterations, 2 threads, and 5 forks)
     *
     * b) Via the Java API:
     */
    public static void main(String[] args) throws RunnerException {
        final Options opt = new OptionsBuilder()
                .include(MicroBenchmark.class.getSimpleName())
                .warmupIterations(2)
                .measurementIterations(2)
                .threads(4)
                .forks(2)
                .build();

        new Runner(opt).run();
    }

    private static class JmxCustomConfig implements JmxConfig {
        @Override
        public String get(String key) {
            return null;
        }

        @Override
        public String prefix() {
            return "micrometrics";
        }

        @Override
        public String domain() {
            return "test-metrics";
        }

        @Override
        public Duration step() {
            return Duration.ofSeconds(10);
        }
    }

    private static ConcurrentMap<String, String> getNewMap(int capacity, int threads) {
        return new ConcurrentHashMap<>(capacity, 0.75f, threads);
    }
}
