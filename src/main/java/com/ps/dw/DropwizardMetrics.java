package com.ps.dw;

import io.dropwizard.metrics5.MetricRegistry;
import io.dropwizard.metrics5.jmx.JmxReporter;
import io.dropwizard.metrics5.jvm.GarbageCollectorMetricSet;

import java.util.concurrent.TimeUnit;

public class DropwizardMetrics {
    private static final MetricRegistry GUS_STREAM = new MetricRegistry();

    public static void main(String[] args) throws InterruptedException {
        GUS_STREAM.registerAll(new GarbageCollectorMetricSet());
        final QueueManager queueManager = new QueueManager(GUS_STREAM);

        final JmxReporter reporter = JmxReporter
                .forRegistry(GUS_STREAM)
                .inDomain("gus-stream")
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start();

        for (int i = 0; i < 100; i++) {
            queueManager.put(i);
            Thread.sleep(1000);
        }

        for (int i = 0; i < 100; i++) {
            queueManager.get();
            Thread.sleep(1000);
        }
    }

    private static class Writer implements Runnable {
        private final QueueManager queueManager;

        private Writer(QueueManager queueManager) {
            this.queueManager = queueManager;
        }

        @Override
        public void run() {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
        }
    }

    private static class Reader implements Runnable {
        private final QueueManager queueManager;

        private Reader(QueueManager queueManager) {
            this.queueManager = queueManager;
        }

        @Override
        public void run() {

        }
    }
}
