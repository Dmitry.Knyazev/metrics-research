package com.ps.dw;

import io.dropwizard.metrics5.*;
import io.dropwizard.metrics5.MetricRegistry;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static io.dropwizard.metrics5.MetricRegistry.name;

public class QueueManager {
    private final Queue<Integer> queue;
    private final Counter counter;
    private final Histogram histogram;
    private final Timer timer;

    public QueueManager(MetricRegistry metrics) {
        this.queue = new ConcurrentLinkedQueue<>();
//        metrics.register(metricName, new QueueMetrics(this.queue));
        this.counter = metrics.counter(name(QueueManager.class, "size"));
        this.histogram = metrics.histogram(name(QueueManager.class, "histo"));
        this.timer = metrics.timer(name(QueueManager.class, "timer"));
    }

    public Integer get() {
        counter.dec();
        Integer poll = this.queue.poll();
        histogram.update(poll);
        return poll;
    }

    public boolean put(Integer v) {
        counter.inc();
        histogram.update(v);
        Timer.Context time = timer.time();
        try {
            return this.queue.offer(v);
        } finally {
            time.stop();
        }

    }
}
