package com.ps.mm;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.jmx.JmxConfig;
import io.micrometer.jmx.JmxMeterRegistry;

import java.time.Duration;

public class MicrometerMetrics {
    public static void main(String[] args) throws InterruptedException {
        Metrics.addRegistry(jmx());
        final QueueManager queueManager = new QueueManager();

        int timeout = 500;
        for (int i = 0; i < 100; i++) {
            queueManager.put(i);
            Thread.sleep(timeout);
        }

        for (int i = 0; i < 100; i++) {
            queueManager.get();
            Thread.sleep(timeout);
        }
    }

    public static JmxMeterRegistry jmx() {
        return new JmxMeterRegistry(
                new JmxCustomConfig(),
                Clock.SYSTEM);
    }

    private static class JmxCustomConfig implements JmxConfig {
        @Override
        public String get(String key) {
            return null;
        }

        @Override
        public String prefix() {
            return "micrometrics";
        }

        @Override
        public String domain() {
            return "gus-stream";
        }

        @Override
        public Duration step() {
            return Duration.ofSeconds(10);
        }
    }
}
