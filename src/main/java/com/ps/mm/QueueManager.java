package com.ps.mm;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.Metrics;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class QueueManager {
    private final Queue<Integer> queue;
    private final Counter counter;
    private final DistributionSummary summary;

    public QueueManager() {
        this.queue = new ConcurrentLinkedQueue<>();
        this.counter = Metrics.counter("queue", "method", "size");
        this.summary = Metrics.summary("queue", "method", "histo");
    }

    public Integer get() {
        this.counter.increment(-1.0D);
        Integer poll = this.queue.poll();
        summary.record(poll);
        return poll;
    }

    public boolean put(Integer v) {
        this.counter.increment();
        this.summary.record(v);
        return this.queue.offer(v);
    }
}
