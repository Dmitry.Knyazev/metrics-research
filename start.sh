#!/usr/bin/env bash
mvn clean install

java -jar target/benchmarks.jar "com.ps.benchmark.MicroBenchmark" -wi 5 -i 5 -t 1 -f 4 | tee threads_1.txt
java -jar target/benchmarks.jar "com.ps.benchmark.MicroBenchmark" -wi 5 -i 5 -t 2 -f 4 | tee threads_2.txt
java -jar target/benchmarks.jar "com.ps.benchmark.MicroBenchmark" -wi 5 -i 5 -t 5 -f 4 | tee threads_5.txt
java -jar target/benchmarks.jar "com.ps.benchmark.MicroBenchmark" -wi 5 -i 5 -t 10 -f 4 | tee threads_10.txt